package com.opendev.testanimation;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ViewAnimator;

import java.util.zip.Inflater;

public class Fragment1 extends Fragment {

    public int height = ViewGroup.LayoutParams.WRAP_CONTENT;
    private int f2Height;
    private LinearLayout linearLayout;

    @Override
    public Animator onCreateAnimator(int transit, final boolean enter, int nextAnim) {
        final String TAG = "asd";
        final int animatorId = (enter) ? R.animator.slide_in_left : R.animator.slide_in_right;
        final Animator anim = AnimatorInflater.loadAnimator(getActivity(), animatorId);
        anim.addListener(new AnimatorListenerAdapter() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onAnimationStart(Animator animation) {
                Log.d(TAG, "onAnimationStart f1 height: " + MainActivity.fragment1.height + " f2 height: " + MainActivity.fragment2.height);
                f2Height = MainActivity.fragment2.height;
                if(height == f2Height) {
                    height = linearLayout.getHeight();
                }
                if(!enter) {
                    MainActivity.isAnimatedStart = true;
                    MainActivity.fragCont.getLayoutParams().height = (height > f2Height) ? height : f2Height;
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                Log.d(TAG, "onAnimationEnd f1");
                if(!enter && height > f2Height) {
                    MainActivity.animCont(MainActivity.fragCont.getLayoutParams().height, f2Height);
                } else {
                    MainActivity.isAnimatedStart = false;
                }
            }
        });

        return anim;

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment1, null);
        linearLayout = (LinearLayout) view.findViewById(R.id.ffR);
        linearLayout.post(new Runnable() {

            @Override
            public void run() {
                height = linearLayout.getHeight();
            }

        });
        return view;
    }

}
