package com.opendev.testanimation;

import android.animation.Animator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.app.Fragment;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

    public static Fragment2 fragment2;
    public static Fragment1 fragment1;
    private FragmentTransaction ft;
    public static boolean isAnimatedStart = false;
    public static FrameLayout fragCont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragment1 = new Fragment1();
        fragment2 = new Fragment2();
        ft = getFragmentManager().beginTransaction();
        fragCont = (FrameLayout) findViewById(R.id.fragCont);

        final FragmentTransaction replace = ft.replace(R.id.fragCont, fragment1);
        ft.addToBackStack(null);

        ft.commit();

        Button btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if(!isAnimatedStart) {
                    ft = getFragmentManager().beginTransaction();
                    if (fragment1.isVisible()) {
                        ft.replace(R.id.fragCont, fragment2);
                    } else {
                        ft.replace(R.id.fragCont, fragment1);
                    }
                    ft.commit();
                }
            }
        });
    }

    public static void animCont(int start, int end) {
        ValueAnimator oa = ValueAnimator.ofInt(start, end)
                .setDuration(300);
        oa.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                Integer value = (Integer) valueAnimator.getAnimatedValue();
                MainActivity.fragCont.getLayoutParams().height = value.intValue();
                MainActivity.fragCont.requestLayout();
            }
        });
        oa.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                MainActivity.isAnimatedStart = false;
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        oa.start();
    }

    private void appHeightCont(FrameLayout frameLayout, int val) {
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) frameLayout.getLayoutParams();
        layoutParams.height = val;
        frameLayout.setLayoutParams(layoutParams);
    }
}